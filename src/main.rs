const MAX_COLS : i32 = 8;
const MAX_ROWS : i32 = 8;
const CELL_CNT : usize = MAX_COLS as usize * MAX_ROWS as usize;

fn draw_screen(generation:i32, g:&[char; CELL_CNT ])
{
    //clear the screen and place the cursor on top
    print!("\x1B[2J\x1B[1;1H");

    println!("Generation : {generation}");

    for n in 0..CELL_CNT
    {
        if n as i32 % MAX_COLS == 0 && n != 0
        {
            println!("");
        }

        print!("{}", g[n]);
    }
}

fn count_neighbors(row :i32, g:&[char;CELL_CNT]) -> i32 {
    let mut neighbor_cnt : i32 = 0;

    let left = row - 1;
    let cntr = row;
    let right = row + 1;

    if row >= 0 && row < CELL_CNT as i32 {

        if left >= 0 && g[left as usize] != ' '{
            neighbor_cnt += 1;
        }
    
        if g[cntr as usize] != ' '{
            neighbor_cnt += 1;
        }
    
        if right < CELL_CNT as i32 && g[right as usize] != ' '{
            neighbor_cnt += 1;
        }
    }

    neighbor_cnt
}
 
fn main() {

    let mut grid : [char ; CELL_CNT] = [' '; CELL_CNT];
    let mut next_gen : [char ; CELL_CNT] = [' '; CELL_CNT];
    let mut generations : i32 = 0;

    //This is our seed. 
    grid[12] = '*';
    grid[19] = '*';
    grid[20] = '*';
    grid[21] = '*';
    grid[28] = '*';

    loop {
        draw_screen(generations, &grid);

        for n in 0..CELL_CNT {
            let cur_row  :i32 = n as i32;
            let prev_row :i32 = cur_row - MAX_COLS;
            let next_row :i32 = cur_row + MAX_COLS;
            let mut neighbor_cnt : i32 = count_neighbors(prev_row, &grid) + count_neighbors(cur_row, &grid) + count_neighbors(next_row, &grid);

            /* Rules:
                Cell is alive if it contains a *, A dead cell is an empty cell

                - Any live cell with two or three live neighbours survives.
                - Any dead cell with three live neighbours becomes a live cell.
                - All other live cells die in the next generation. Similarly, all other dead cells stay dead.
            */
            if grid[n] != ' ' {

                //If we started alive, we counted our self when we did the center row
                //so just substract one from the count
                neighbor_cnt -= 1;

                if neighbor_cnt == 2 || neighbor_cnt == 3{
                    next_gen[n] = '*';
                }else{
                    next_gen[n] = ' ';
                }
            }else {
                if neighbor_cnt == 3{
                    next_gen[n] = '*';
                }else{
                    next_gen[n] = ' ';
                }
            }
        }

        grid = next_gen;
        generations += 1;
    }
}
